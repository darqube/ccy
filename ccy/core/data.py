def make_ccys(db):
    """
    Create the currency dictionary
    """
    dfr = 4
    dollar = r"\u0024"
    peso = r"\u20b1"
    kr = r"kr"
    insert = db.insert

    # G10 & SCANDI
    insert("EUR", "978", "EU", 1, "Euro", dfr, "EU", "30/360", "ACT/360", future="FE", symbol=r"\u20ac", html="&#x20ac;")
    insert("GBP", "826", "BP", 2, "British Pound", dfr, "GB", "ACT/365", "ACT/365", symbol=r"\u00a3", html="&#xa3;")
    insert("AUD", "036", "AD", 3, "Australian Dollar", dfr, "AU", "ACT/365", "ACT/365", symbol=dollar, html="&#x24;")
    insert("NZD", "554", "ND", 4, "New-Zealand Dollar", dfr, "NZ", "ACT/365", "ACT/365", symbol=dollar, html="&#x24;")
    insert("USD", "840", "UD", 5, "US Dollar", 0, "US", "30/360", "ACT/360", future="ED", symbol=dollar, html="&#x24;")
    insert("CAD", "124", "CD", 6, "Canadian Dollar", dfr, "CA", "ACT/365", "ACT/365", symbol=dollar, html="&#x24;")
    insert("CHF", "756", "SF", 7, "Swiss Franc", dfr, "CH", "30/360", "ACT/360", symbol=r"Fr", html="&#x20a3;")
    insert("NOK", "578", "NK", 8, "Norwegian Krona", dfr, "NO", "30/360", "ACT/360", symbol=kr, html=kr)
    insert("SEK", "752", "SK", 9, "Swedish Krona", dfr, "SE", "30/360", "ACT/360", symbol=kr, html=kr)
    insert("DKK", "208", "DK", 10, "Danish Krona", dfr, "DK", "30/360", "ACT/360", symbol=kr, html=kr)
    insert("JPY", "392", "JY", 10000, "Japanese Yen", 0, "JP", "ACT/365", "ACT/360", symbol=r"\u00a5", html="&#xa5;")

    # ASIA
    # FIXME: CNH iso number 157 is a work around
    insert("CNH", "157", "CR", 680, "Offshore Chinese Renminbi", dfr, "CN", "ACT/365", "ACT/365", symbol=r"\u00a5", html="&#xa5;")
    insert("CNY", "156", "CY", 680, "Chinese Renminbi", dfr, "CN", "ACT/365", "ACT/365", symbol=r"\u00a5", html="&#xa5;")
    insert("KRW", "410", "KW", 110000, "South Korean won", 0, "KR", "ACT/365", "ACT/365", symbol=r"\u20a9", html="&#x20a9;")
    insert("SGD", "702", "SD", 15, "Singapore Dollar", dfr, "SG", "ACT/365", "ACT/365", symbol=dollar, html="&#x24;")
    insert("IDR", "360", "IH", 970000, "Indonesian Rupiah", 0, "ID", "ACT/360", "ACT/360", symbol=r"Rp", html="Rp")
    insert("THB", "764", "TB", 3300, "Thai Baht", 2, "TH", "ACT/365", "ACT/365", symbol=r"\u0e3f", html="&#xe3f;")
    insert("TWD", "901", "TD", 18, "Taiwan Dollar", dfr, "TW", "ACT/365", "ACT/365", symbol=dollar, html="&#x24;")
    insert("HKD", "344", "HD", 19, "Hong Kong Dollar", dfr, "HK", "ACT/365", "ACT/365", symbol=r"\u5713", html="HK&#x24;")
    insert("PHP", "608", "PP", 4770, "Philippines Peso", dfr, "PH", "ACT/360", "ACT/360", symbol=peso, html="&#x20b1;")
    insert("INR", "356", "IR", 4500, "Indian Rupee", dfr, "IN", "ACT/365", "ACT/365", symbol=r"\u20a8", html="&#x20a8;")
    insert("MYR", "458", "MR", 345, "Malaysian Ringgit", dfr, "MY", "ACT/365", "ACT/365")
    insert("VND", "704", "VD", 1700000, "Vietnamese Dong", 0, "VN", "ACT/365", "ACT/365", symbol=r"\u20ab", html="&#x20ab;")

    # LATIN AMERICA
    insert("BRL", "986", "BC", 200, "Brazilian Real", dfr, "BR", "BUS/252", "BUS/252", symbol=r"R$")
    insert("PEN", "604", "PS", 220, "Peruvian New Sol", dfr, "PE", "ACT/360", "ACT/360", symbol=r"S/.")
    insert("ARS", "032", "AP", 301, "Argentine Peso", dfr, "AR", "30/360", "ACT/360", symbol=dollar, html="&#x24;")
    insert("MXN", "484", "MP", 1330, "Mexican Peso", dfr, "MX", "ACT/360", "ACT/360", symbol=dollar, html="&#x24;")
    insert("CLP", "152", "CH", 54500, "Chilean Peso", 2, "CL", "ACT/360", "ACT/360", symbol=dollar, html="&#x24;")
    insert("COP", "170", "CL", 190000, "Colombian Peso", 2, "CO", "ACT/360", "ACT/360", symbol=dollar, html="&#x24;")
    # TODO: Check towletters code and position
    insert("JMD", "388", "JM", 410, "Jamaican Dollar", dfr, "JM", "ACT/360", "ACT/360", symbol=dollar, html="&#x24;")
    # TODO: Check towletters code and position
    insert("TTD", "780", "TT", 410, "Trinidad and Tobago Dollar", dfr, "TT", "ACT/360", "ACT/360", symbol=dollar, html="&#x24;")
    # TODO: Check towletters code and position
    insert("BMD", "060", "BM", 410, "Bermudian Dollar", dfr, "BM", symbol=dollar, html="&#x24;")
    insert("FJD", "242", "FD", 410, "Fiji dollar", dfr, "FJ", symbol=dollar, html="&#x24;")

    # EASTERN EUROPE
    insert("CZK", "203", "CK", 28, "Czech Koruna", dfr, "CZ", "ACT/360", "ACT/360", symbol=r"\u004b\u010d")
    insert("PLN", "985", "PZ", 29, "Polish Zloty", dfr, "PL", "ACT/ACT", "ACT/365", symbol=r"\u0050\u0142")
    insert("TRY", "949", "TY", 30, "Turkish Lira", dfr, "TR", "ACT/360", "ACT/360", symbol=r"\u0054\u004c")
    insert("HUF", "348", "HF", 32, "Hungarian Forint", dfr, "HU", "ACT/365", "ACT/360", symbol=r"Ft", html="Ft")
    insert("RON", "946", "RN", 34, "Romanian Leu", dfr, "RO", "ACT/360", "ACT/360")
    insert("UAH", "980", "UH", 35, "Ukrainian Hryvnia", dfr, "UA", "ACT/ACT", "ACT/ACT", symbol=r"\u20b4", html="&#x20b4;")
    insert("RUB", "643", "RR", 36, "Russian Ruble", dfr, "RU", "ACT/ACT", "ACT/ACT", symbol=r"\u0440\u0443\u0431")
    # TODO: Check towletters code and position
    insert("HRK", "191", "HK", 410, "Croatian kuna", dfr, "HR", symbol=r"kn")
    # TODO: Check towletters code and position
    insert("KZT", "398", "KT", 410, "Tenge", dfr, "KZ", symbol=r"\u20b8", html="&#x20b8;")
    # TODO: Check towletters code and position
    insert("BGN", "975", "BL", 410, "Bulgarian Lev", dfr, "BG", symbol=r"\u043b\u0432.", html="&#1083;&#1074;")

    # MIDDLE EAST & AFRICA
    insert("ILS", "376", "IS", 410, "Israeli Shekel", dfr, "IL", "ACT/365", "ACT/365", symbol=r"\u20aa", html="&#x20aa;")
    # TODO: Check towletters code and position
    insert("AED", "784", "AE", 410, "United Arab Emirates Dirham", 2, "AE")
    insert("BHD", "048", "BH", 410, "Bahraini Dinar", 2, "BH", symbol="BD")
    insert("BDT", "050", "BT", 410, "Bangladeshi Taka", 2, "BD", symbol=r"p", html="&#2547;")
    insert("BWP", "072", "BW", 410, "Botswana Pula", 2, "BW", symbol=r"p", html="&#2547;")
    # TODO: Check towletters code and position
    insert("QAR", "634", "QA", 410, "Qatari Riyal", 2, "QA", symbol=r"\ufdfc", html="&#xfdfc;")
    insert("PKR", "586", "PR", 410, "Pakistani Rupee", 2, "PK", "ACT/365", "ACT/365", symbol=r"Rs", html="Rs")
    insert("ISK", "352", "IK", 410, "Icelandic Krona", 0, "IS", "ACT/365", "ACT/365", symbol=r"Kr", html="Kr")
    insert("CDF", "976", "FC", 410, "Congolese Franc", 2, "CD", "ACT/365", "ACT/365", symbol=r"FC", html="FC")
    insert("CRC", "188", "CC", 410, "Costa Rican Colon", 2, "CR", "ACT/365", "ACT/365", symbol=r"₡", html="&#8353;)")
    insert("CUP", "192", "CP", 410, "Cuban Peso", 2, "CU", "ACT/365", "ACT/365", symbol=r"$MN", html="$MN")
    insert("DJF", "262", "DF", 410, "Djiboutian Franc", 2, "DJ", "ACT/365", "ACT/365", symbol=r"Fdj", html="Fdj")
    insert("AMD", "051", "AM", 410, "Armenian Dram", 2, "AM", "ACT/365", "ACT/365", symbol=r"դր", html="&#1423;")
    insert("BBD", "052", "BB", 410, "Barbados Dollar", 2, "BB", "ACT/365", "ACT/365", symbol=r"BBD$", html="BBD$")
    insert("BZD", "084", "BZ", 410, "Belize Dollar", 2, "BZ", "ACT/365", "ACT/365", symbol=r"$", html="$")
    insert("BOB", "068", "BO", 410, "Boliviano", 2, "BO", "ACT/365", "ACT/365", symbol=r"Bs", html="Bs")
    insert("BAM", "977", "BA", 410, "Bosnia and Herzegovina Convertible Mark", 2, "BA", "ACT/365", "ACT/365", symbol=r"KM", html="KM")
    insert("BND", "096", "BN", 410, "Brunei Dollar", 2, "BN", "ACT/365", "ACT/365", symbol=r"B$", html="B$")
    insert("BIF", "108", "BF", 410, "Burundian Franc", 2, "BI", "ACT/365", "ACT/365", symbol=r"FBu", html="FBu")
    insert("CVE", "132", "CE", 410, "Cape Verdean Escudo", 2, "CV", "ACT/365", "ACT/365", symbol=r"$", html="$")
    insert("KHR", "116", "KR", 410, "Cambodian Riel", 2, "KH", "ACT/365", "ACT/365", symbol=r"៛", html="KHR")
    insert("SVC", "222", "SC", 410, "Salvadoran Colon", 2, "SV", "ACT/365", "ACT/365", symbol=r"₡", html="&#8353;")
    insert("ETB", "230", "EB", 410, "Ethiopian Birr", 2, "ET", "ACT/365", "ACT/365", symbol=r"Br", html="Br")
    insert("SZL", "748", "SL", 410, "Swazi Lilangeni", 2, "SZ", "ACT/365", "ACT/365", symbol=r"L", html="L")
    insert("KWD", "414", "KD", 410, "Kuwaiti Dinar", 3, "KW", "ACT/365", "ACT/365", symbol=r"د.ك", html="KD")
    insert("UZS", "860", "UZ", 410, "Uzbekistan Som", 2, "UZ", "ACT/365", "ACT/365", symbol=r"сум", html="сум")
    insert("ALL", "008", "AL", 410, "Albanian Lek", 2, "AL", "ACT/365", "ACT/365", symbol=r"L", html="L")
    insert("AFN", "971", "AF", 410, "Afghan Afghani", 2, "AF", "ACT/365", "ACT/365", symbol=r"Af", html="Af")
    insert("DOP", "214", "DP", 410, "Dominican Peso", 2, "DO", "ACT/365", "ACT/365", symbol=r"RD$", html="RD$")
    insert("BSD", "044", "BD", 410, "Bahamian Dollar", 2, "BS", "ACT/365", "ACT/365", symbol=r"B$", html="B$")
    insert("HNL", "340", "HL", 410, "Honduran Lempira", 2, "HN", "ACT/365", "ACT/365", symbol=r"L", html="L")
    insert("GTQ", "320", "GQ", 410, "Guatemalan Quetzal", 2, "GT", "ACT/365", "ACT/365", symbol=r"Q", html="Q")
    insert("JOD", "400", "JD", 410, "Jordanian Dinar", 3, "JO", "ACT/365", "ACT/365", symbol=r"د.أ", html="JD")
    insert("LSL", "426", "LL", 410, "Lesotho Loti", 2, "LS", "ACT/365", "ACT/365", symbol=r"L", html="L")
    insert("MMK", "104", "MK", 410, "Myanmar Kyat", 2, "MM", "ACT/365", "ACT/365", symbol=r"K", html="K")
    insert("MKD", "807", "MD", 410, "Macedonian Denar", 2, "MK", "ACT/365", "ACT/365", symbol=r"den", html="den")
    insert("LYD", "434", "LD", 410, "Libyan Dinar", 3, "LY", "ACT/365", "ACT/365", symbol=r"ل.د", html="LD")
    insert("GNF", "324", "GF", 410, "Guinean Franc", 0, "GN", "ACT/365", "ACT/365", symbol=r"GFr", html="GFr")
    insert("MDL", "498", "ML", 410, "Moldovan Leu", 2, "MD", "ACT/365", "ACT/365", symbol=r"L", html="L")
    insert("IQD", "368", "ID", 410, "Iraqi Dinar", 3, "IQ", "ACT/365", "ACT/365", symbol=r"د.ع", html="D")
    insert("LKR", "144", "LR", 410, "Sri Lankan Rupee", 2, "LK", "ACT/365", "ACT/365", symbol=r"₨", html="Rs")
    insert("NAD", "516", "DN", 410, "Namibian Dollar", 2, "NA", "ACT/365", "ACT/365", symbol=r"N$", html="N$")
    insert("OMR", "512", "OR", 410, "Omani Rial", 3, "OM", "ACT/365", "ACT/365", symbol=r"R.O.", html="R.O.")
    insert("LBP", "422", "LP", 410, "Lebanese Pound", 2, "LB", "ACT/365", "ACT/365", symbol=r"LL", html="LL")
    insert("LAK", "418", "LK", 410, "Lao Kip", 2, "LA", "ACT/365", "ACT/365", symbol=r"₭N", html="KN")
    insert("KYD", "136", "KY", 410, "Cayman Islands Dollar", 2, "KY", "ACT/365", "ACT/365", symbol=dollar, html="&#x24;")
    insert("MAD", "504", "DH", 410, "Moroccan Dirham", 2, "MA", "ACT/365", "ACT/365", symbol=r"DH", html="DH")
    insert("RSD", "941", "RD", 410, "Serbian Dinar", 2, "RS", "ACT/365", "ACT/365", symbol=r"din", html="din")
    insert("HTG", "332", "HG", 410, "Haitian Gourde", 2, "HT", "ACT/365", "ACT/365", symbol=r"G", html="G")
    insert("PYG", "600", "PG", 410, "Paraguayan Guarani", 0, "PY", "ACT/365", "ACT/365", symbol=r"\u20B2", html="&#8370;")
    insert("MWK", "454", "WK", 410, "Malawian Kwacha", 2, "MW", "ACT/365", "ACT/365", symbol=r"K", html="K")
    insert("PGK", "598", "PK", 410, "Papua New Guinean Kina", 2, "PG", "ACT/365", "ACT/365", symbol=r"K", html="K")
    insert("MOP", "446", "PM", 410, "Macanese Pataca", 2, "MO", "ACT/365", "ACT/365", symbol=r"MOP\u0024", html="MOP&#x24;")
    insert("SOS", "706", "SS", 410, "Somali Shilling", 2, "SO", "ACT/365", "ACT/365", symbol=r"Sh.So.", html="Sh.So.")
    insert("NIO", "558", "NO", 410, "Nicaraguan Cordoba", 2, "NI", "ACT/365", "ACT/365", symbol=r"C\u0024", html="C&#x24;")
    insert("DZD", "012", "DA", 410, "Algerian Dinar", 2, "DZ", "ACT/365", "ACT/365", symbol=r"DA", html="DA")
    insert("KES", "404", "KS", 410, "Kenyan Shilling", 2, "KE", "ACT/365", "ACT/365", symbol=r"KSh", html="KSh")
    insert("MUR", "480", "RM", 410, "Mauritian Rupee", 2, "MU", "ACT/365", "ACT/365", symbol=r"Rs", html="Rs")
    insert("RWF", "646", "RF", 410, "Rwandan Franc", 0, "RW", "ACT/365", "ACT/365", symbol=r"R₣", html="RF")
    insert("UYU", "858", "UP", 410, "Uruguayan Peso", 2, "UY", "ACT/365", "ACT/365", symbol=r"\u0024U", html="U")
    insert("MVR", "462", "FR", 410, "Maldivian Rufiyaa", 2, "MV", "ACT/365", "ACT/365", symbol=r"Rf", html="Rf")
    insert("SCR", "690", "SR", 410, "Seychelles Rupee", 2, "SC", "ACT/365", "ACT/365", symbol=r"Rs", html="Rs")
    insert("NPR", "524", "NR", 410, "Nepalese Rupee", 2, "NP", "ACT/365", "ACT/365", symbol=r"Rs", html="Rs")
    insert("UGX", "800", "UX", 410, "Ugandan Shilling", 0, "UG", "ACT/365", "ACT/365", symbol=r"USh", html="USh")
    insert("GHS", "936", "GH", 410, "Ghanaian Cedi", 2, "GH", "ACT/365", "ACT/365", symbol=r"GH\u20B5", html="GH&#x20B5;")
    insert("TND", "788", "DT", 410, "Tunisian Dinar", 3, "TN", "ACT/365", "ACT/365", symbol=r"DT", html="DT")
    insert("PAB", "590", "PB", 410, "Panamanian Balboa", 2, "PA", "ACT/365", "ACT/365", symbol=r"B/.", html="B/.")
    insert("YER", "886", "YR", 410, "Yemeni Rial", 2, "YE", "ACT/365", "ACT/365", symbol=r"YR", html="YR")
    insert("TZS", "834", "TS", 410, "Tanzanian Shilling", 3, "TZ", "ACT/365", "ACT/365", symbol=r"TSh", html="TSh")
    insert("SLL", "694", "LE", 410, "Sierra Leonean Leone", 2, "SL", "ACT/365", "ACT/365", symbol=r"Le", html="Le")
    insert("GMD", "270", "GD", 410, "Gambian Dalasi", 2, "GM", "ACT/365", "ACT/365", symbol=r"D", html="D")
    insert("GEL", "981", "GL", 410, "Georgian Lari", 2, "GE", "ACT/365", "ACT/365", symbol=r"GL", html="GL")
    insert("TOP", "776", "PT", 410, "Tongan Pa'anga", 2, "TO", "ACT/365", "ACT/365", symbol=r"T\u0024", html="T&#x24;")
    insert("BYN", "933", "BR", 410, "Belarusian Ruble", 2, "BY", "ACT/365", "ACT/365", symbol=r"Br", html="Br")
    insert("LRD", "430", "DL", 410, "Liberian dollar", 2, "LR", "ACT/365", "ACT/365", symbol=r"L\u0024", html="L&#x24;")
    insert("KMF", "174", "CF", 410, "Comoro Franc", 0, "KM", "ACT/365", "ACT/365", symbol=r"CF", html="CF")

    # TODO: Check towletters code and position
    insert("SAR", "682", "RS", 410, "Saudi Riyal", dfr, "SA", symbol=r"\ufdfc", html="&#xfdfc;")
    insert("EGP", "818", "EP", 550, "Egyptian Pound", dfr, "EG", symbol=r"\u00a3", html="&#xa3;")
    insert("NGN", "566", "NG", 650, "Nigerian Naira", dfr, "NG", symbol=r"\u20a6", html="&#x20A6;")
    insert("ZAR", "710", "SA", 750, "South African Rand", dfr, "ZA", "ACT/365", "ACT/365", symbol=r"R", html="R")
    insert("ZWL", "932", "ZW", 410, "Zimbabwean dollar", 2, "ZW", symbol=r"Z$", html="Z$")
